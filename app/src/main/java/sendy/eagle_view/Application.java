package sendy.eagle_view;

import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParsePush;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Derrick Nyakiba on 10/15/2015
 * derricknyakiba@gmail.com
 * Last Modified:10/15/2015 by Derrick Nyakiba
 * Main class started at start up
 */


public class Application extends android.app.Application{



    @Override
    public void onCreate() {
        super.onCreate();
        //Set up parse for notifications
        Parse.initialize(this, "ENePLfpg0Erleu2ZPr2jFlrUBH0LsrEzD7lH7ehV", "NLsl2S8NqYH9ocQ1xc3iChRGX8IZGyrYNuMzSWL9");
        ParsePush.subscribeInBackground("sendy");
        ParseInstallation.getCurrentInstallation().saveInBackground();

        RealmConfiguration config = new RealmConfiguration.Builder(this)
                .name("sendyrider.realm")
                .schemaVersion(1)
                .build();
        Realm.setDefaultConfiguration(config);
    }
}
