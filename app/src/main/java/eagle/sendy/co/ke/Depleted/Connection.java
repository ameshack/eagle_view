package eagle.sendy.co.ke.Depleted;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONObject;

/**
 * Created by Derrick Nyakiba on 10/15/2015
 * derricknyakiba@gmail.com
 * Last Modified:10/15/2015 by Derrick Nyakiba
 *
 */
public class Connection {

    public static final MediaType JSON  = MediaType.parse("application/json; charset=utf-8");

    public String HandShake(String requestPayload){
        String result = "";
        try {
            JSONObject jsonObjectRequest=new JSONObject(requestPayload);//get the object from string containing data
            String json;
            json = jsonObjectRequest.getJSONObject("request").toString();//get the request to be made in string format
            OkHttpClient client = new OkHttpClient();
            RequestBody body = RequestBody.create(JSON, json);
            Request request = new Request.Builder()  //create the request
                    .url(jsonObjectRequest.getString("url"))
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute(); //exexcute the request and get response

            result= response.body().string();//return the result from the server in string format
        }
        catch (Exception e) {
            System.out.println("InputStream"+ e.getLocalizedMessage());
            System.out.println("InputStream Error "+ e);
        }
        System.out.println("result"+ result);
        return result;
    }
}
