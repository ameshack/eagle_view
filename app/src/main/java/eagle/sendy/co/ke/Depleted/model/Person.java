package eagle.sendy.co.ke.Depleted.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Derrick Nyakiba on 10/15/2015
 * derricknyakiba@gmail.com
 * Last Modified:10/15/2015 by Derrick Nyakiba
 */
public class Person extends RealmObject {

    @PrimaryKey
    private int rider_id;
    private String name;
    private String phone;
    private boolean ondelivery;

    public int getRider_id() {
        return rider_id;
    }

    public void setRider_id(int rider_id) {
        this.rider_id = rider_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }



    public boolean isOndelivery() {
        return ondelivery;
    }

    public void setOndelivery(boolean ondelivery) {
        this.ondelivery = ondelivery;
    }
}
