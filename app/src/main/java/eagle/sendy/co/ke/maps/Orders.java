package eagle.sendy.co.ke.maps;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import org.json.JSONArray;
import org.json.JSONObject;

import eagle.sendy.co.ke.helpus.MyConstants;

/**
 * Created by Derrick Nyakiba on 10/15/2015
 * derricknyakiba@gmail.com
 * Last Modified:10/28/2015 by Derrick Nyakiba
 * Class handling the orders made
 */


public class Orders{

    public Marker marker;
    private String order_no;
    private String name;
    private String phone;
    public int conf = 0;

    public void addMarker(JSONObject value ){
        try{
            this.name = value.getString("name");
            this.order_no = value.getString("order_no");
            this.phone = value.getString("phone");
            JSONArray positions = value.getJSONArray("position");
            JSONObject position = positions.getJSONObject(0);
            double lat = position.getDouble("lat");
            double lng = position.getDouble("lng");
            System.out.println("The name is " + name);
            LatLng place = new LatLng(lat,lng);

            MarkerOptions mark = new MarkerOptions()
                                    .title(name).position(place).alpha(0.7f)
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));

            marker = MyConstants.googleMap.addMarker(mark);
            System.out.println("The order no is " + order_no);

            Thread a = new Thread(){
                public void run(){
                    try{
                        sleep(300000);
                        if(conf == 0){
                            MyConstants.rem.add(order_no);
                        }
                    }
                    catch(Exception e){
                        e.printStackTrace();
                    }
                }
            };
            a.start();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
    public String getOrder_no(){
        return this.order_no;
    }

    public void confirmMarker(JSONObject value){
        conf = 1;

        try{
            this.order_no = value.getString("order_no");
            marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));

        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public void removeMarker(){
        marker.remove();
    }

}
