package eagle.sendy.co.ke.Depleted;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.Comparator;
import java.util.Date;

import eagle.sendy.co.ke.helpus.MyConstants;

/**
 * This class carries data corresponding to a rider's location at a particular time
 *
 * Created by Jason Rogena: jasonrogena@gmail.com
 */
public class Point {
    private static final String KEY_LAT = "lat";
    private static final String KEY_LNG = "long";
    private static final String KEY_COURSE = "course";
    private static final String KEY_TIME = "time";

    public final LatLng latLng;
    private final long course;
    private final Date time;

    public Point(JSONObject pointObject) throws JSONException, ParseException{
        this.latLng = new LatLng(pointObject.getDouble(KEY_LAT), pointObject.getDouble(KEY_LNG));
        this.course = pointObject.getLong(KEY_COURSE);
        this.time = MyConstants.sdf2.parse(pointObject.getString(KEY_TIME));
    }

    /**
     * Returns the time represented by this point
     *
     * @return The point's time
     * @see java.util.Date
     */
    public Date getTime() {
        return this.time;
    }

    public LatLng getLatLng() {
        return  this.latLng;
    }

    /**
     * This class is used to compare two points. Can be used with Collections.sort
     *
     * @see java.util.Collections
     */
    public static class PointComparator implements Comparator<Point> {

        @Override
        public int compare(Point p0, Point p1) {
            long t0 = p0.getTime().getTime();
            long t1 = p1.getTime().getTime();

            if(t0 < t1){
                return -1;
            }
            else if(t0 == t1){
                return 0;
            }
            else {
                return 1;
            }
        }
    }
}