package eagle.sendy.co.ke.maps;

import android.graphics.Color;
import android.os.Handler;
import android.os.SystemClock;
import android.view.animation.LinearInterpolator;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;

import eagle.sendy.co.ke.helpus.MyConstants;
import sendy.eagle_view.R;

/**
 * Created by Derrick Nyakiba on 10/19/2015
 * derricknyakiba@gmail.com
 * Last Modified:10/28/2015 by Derrick Nyakiba
 * Class that handles the rider locations and number of riders per region
 */
public class Riders {

    int rider_id;
    String name;
    String phone;
    Boolean onDelivery;
    private Marker marker;
    int across = -1;
    int width = -1;
    int course;
    LatLng startLatLong;
    public void addRider(JSONObject values){
        startLatLong = new LatLng(MyConstants.startLat,MyConstants.startLong);
        try{
            rider_id = values.getInt("rider_id");
            onDelivery = values.getBoolean("on_delivery");
            name = values.getString("name");
            JSONObject position = values.getJSONObject("position");
            double lat = position.getDouble("lat");
            double lng = position.getDouble("lng");
            course = position.getInt("course");
            LatLng ourPoint = new LatLng(lat,lng);
            int a = thisLat(lat);
            int c = thisLong(lng);
            if(across == -1 || width == -1 ){
                MyConstants.ourArray[a][c]++;
                across = a;
                width = c;
            }
            else if(across !=a || width != c ){
                MyConstants.ourArray[across][width]--;
                MyConstants.ourArray[a][c]++;
                across = a;
                width = c;
            }

            if(course <=180){
                marker = MyConstants.googleMap.addMarker(new MarkerOptions().position(ourPoint)
                        .title(this.name)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.moto_right_32))
                        .rotation(course - 90)
                        .alpha(0.7f));

            }
            else{
                marker = MyConstants.googleMap.addMarker(new MarkerOptions().position(ourPoint)
                        .title(this.name)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.moto_left_32))
                        .rotation(course - 270)
                        .alpha(0.7f));
            }
            marker.setAnchor(0.5f,0.5f);

            if(onDelivery){
                marker.setTitle(name + " is on delivery");
            }
            else {
                marker.setTitle(name + " is free");
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        makePaint();
    }
    public void loadMarker(JSONObject values){
        try{
            onDelivery = values.getBoolean("on_delivery");
            JSONObject position = values.getJSONObject("position");
            double lat = position.getDouble("lat");
            double lng = position.getDouble("lng");
            course = position.getInt("course");
            LatLng newPoint = new LatLng(lat,lng);
            int a = thisLat(lat);
            int c = thisLong(lng);
            if( a != -1 && c != -1){
                if(across == -1 || width == -1 ){
                    MyConstants.ourArray[a][c]++;
                    across = a;
                    width = c;
                }
                else if(across !=a || width != c ){
                    MyConstants.ourArray[across][width]--;
                    MyConstants.ourArray[a][c]++;
                    across = a;
                    width = c;
                }
            }
            moveMarker(newPoint,2000);
            makePaint();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public int thisLat(double c){
        for(int i = 1 ; i <= MyConstants.height ; i++){
            double fin = MyConstants.startLat + (i*MyConstants.divLat);
            if(c>MyConstants.startLat && c < fin){
                return (MyConstants.height-i);
            }
        }
        return -1;
    }
    public int thisLong(double c){
        for(int i = 1 ; i <= MyConstants.width ; i++){
            double fin = MyConstants.startLong + (i*MyConstants.divLong);
            if(c>MyConstants.startLat && c < fin){
                return i-1;
            }
        }
        return -1;
    }

    private void moveMarker(final LatLng toLatLng, final long duration) {
        System.out.println("Moving marker to " + toLatLng.toString() + " in " + duration + " for " + this.name);
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final LatLng startLatLng = marker.getPosition();
        final LinearInterpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {

                if(course <=180){
                    marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.moto_right_32));
                    marker.setRotation(course - 90);
                }
                else{
                    marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.moto_right_32));
                    marker.setRotation(course - 270);
                }
                if(onDelivery){
                    marker.setTitle(name + " is on delivery");
                }
                else{
                    marker.setTitle(name + " is free");
                }
                long elapsed = SystemClock.uptimeMillis() - start;

                float t = interpolator.getInterpolation((float) elapsed / duration);
                double lng = t * toLatLng.longitude + (1 - t) * startLatLng.longitude;
                double lat = t * toLatLng.latitude + (1 - t) * startLatLng.latitude;
                LatLng newPoint = new LatLng(lat, lng);
                if(marker.getPosition() != newPoint){
                    marker.setPosition(newPoint);
                }
                marker.setAnchor(0.5f,0.5f);
                if (t < 1.0) {
                    handler.postDelayed(this, 16);
                }
            }
        });
    }
    public void paintMe(int i,int j,int add){
        int a = 0x40ff0000;
        System.out.println("The color is " + MyConstants.polygon[i][j].getFillColor() + " while a is " + a);
        Boolean d = MyConstants.polygon[i][j].getFillColor() == a;
        if(add == 1 && !d){
            MyConstants.polygon[i][j].setFillColor(0x40ff0000);
        }
        else if(add == 0){
            MyConstants.polygon[i][j].setFillColor(Color.TRANSPARENT);
        }
    }
    public void makePaint(){
        for(int i = 0 ; i < MyConstants.height ; i++ ){
            for( int j = 0 ; j < MyConstants.width ; j++ ){
                if(MyConstants.ourArray[i][j]==0){
                    paintMe(i,j,1);
                }
                else{
                    paintMe(i,j,0);
                }
            }
        }
    }
    public int getRider_id(){
        return this.rider_id;
    }
}
