package eagle.sendy.co.ke.handshake;

import android.content.Context;
import android.content.Intent;

import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import eagle.sendy.co.ke.helpus.MyConstants;
import eagle.sendy.co.ke.maps.Orders;
import eagle.sendy.co.ke.maps.Riders;

/**
 * Created by Derrick Nyakiba on 10/16/2015
 * derricknyakiba@gmail.com
 * Last Modified:10/28/2015 by Derrick Nyakiba
 *
 * Class to receive parse data from the server regarding rider location and order requests
 */

public class Receiver extends ParsePushBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        System.out.println("Parse notification received");


        try{
            JSONObject a = new JSONObject(intent.getStringExtra("com.parse.Data"));
            if (a.getString("type").equals("order")){//check if it is an order
                JSONArray values = a.getJSONArray("value");
                JSONObject value = values.getJSONObject(0);
                if(a.getString("status").equals("Requested")){
                    long tim = System.currentTimeMillis();
                    Orders order = new Orders();
                    System.out.println("Order has been made");
                    MyConstants.ourOrders.add(order);
                    MyConstants.ourOrders.get(MyConstants.place).addMarker(value);
                    MyConstants.place++;
                }
                else if(a.getString("status").equals("Confirmed")){
                    int m = 0;
                    String order = value.getString("order_no");
                    System.out.println("Order has been confirmed " + order);
                    for(int i = 0 ; i < MyConstants.place ; i++ ){
                        if(MyConstants.ourOrders.get(i).getOrder_no().equals(order)){
                            MyConstants.ourOrders.get(i).confirmMarker(value);
                            System.out.println("The order has been found");
                            m = 1;
                        }
                    }
                    if(m==0){
                        Orders orer = new Orders();
                        MyConstants.ourOrders.add(orer);
                        MyConstants.ourOrders.get(MyConstants.place).addMarker(value);
                        MyConstants.ourOrders.get(MyConstants.place).confirmMarker(value);
                        MyConstants.place++;
                    }

                }
                else if(a.getString("status").equals("Completed")){
                    String order = value.getString("order_no");
                    for(int i = 0 ; i < MyConstants.place ; i++ ){
                        if(MyConstants.ourOrders.get(i).getOrder_no().equals(order)){
                            MyConstants.ourOrders.get(i).removeMarker();
                            MyConstants.ourOrders.remove(i);
                        }
                    }
                }
            }
            else if(a.getString("type").equals("riders")){
                JSONObject value = a.getJSONObject("value");
                int rider_id = value.getInt("rider_id");
                int found = 0;
                for(int i = 0 ; i < MyConstants.riderPlace ; i++){
                    if(MyConstants.riders.get(i).getRider_id() == rider_id ){
                        MyConstants.riders.get(i).loadMarker(value);
                        found = 1;
                    }
                }
                if(found == 0){
                    Riders rider = new Riders();
                    MyConstants.riders.add(rider);
                    MyConstants.riders.get(MyConstants.riderPlace).addRider(value);
                    MyConstants.riderPlace++;
                }
            }
            removeUnconfirmed();
        }
        catch(JSONException e){
            e.printStackTrace();
        }
    }


    public void removeUnconfirmed(){//function to remove the requested orders that have not been confirmed
        if(MyConstants.rem.size()>0){
            for(int i = 0 ; i < MyConstants.rem.size();i++){
                for(int j = 0 ; j < MyConstants.place ; j++){
                    if(MyConstants.ourOrders.get(j).getOrder_no().equals(MyConstants.rem.get(i))){
                        MyConstants.ourOrders.get(j).removeMarker();
                        MyConstants.ourOrders.remove(j);
                    }
                }

            }
        }
    }
}