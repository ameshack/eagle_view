package eagle.sendy.co.ke.Depleted;


        import android.graphics.Color;
        import android.os.Handler;
        import android.os.SystemClock;
        import android.util.Log;
        import android.view.animation.LinearInterpolator;

        import com.google.android.gms.maps.GoogleMap;
        import com.google.android.gms.maps.model.BitmapDescriptorFactory;
        import com.google.android.gms.maps.model.LatLng;
        import com.google.android.gms.maps.model.Marker;
        import com.google.android.gms.maps.model.MarkerOptions;
        import com.google.android.gms.maps.model.Polyline;
        import com.google.android.gms.maps.model.PolylineOptions;

        import java.text.ParseException;
        import java.util.ArrayList;
        import java.util.Collections;
        import java.util.Date;
        import java.util.List;

        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        import eagle.sendy.co.ke.Depleted.Point;
        import eagle.sendy.co.ke.helpus.MyConstants;
        import sendy.eagle_view.R;

/**
 * This class carries information on a rider. It also handles all events that are rider specific
 *
 * Created by Jason Rogena: jasonrogena@gmail.com
 */
public class Riders{

    private static final String TAG = "Sendy.Rider";
    private static final String KEY_NAME = "name";
    private static final String KEY_ID = "rider_id";
    private static final String KEY_ONDELIVERY = "ondelivery";
    private static final String KEY_POSITIONS = "positions";

    private ArrayList<Point> points;
    private String name;
    private int id;
    private  boolean ondeliver;
    private Marker marker;
    private Polyline polyline;
    private List<LatLng> drawnLine;
    private GoogleMap googleMap;
    private int lastViewedPointIndex;
    private long calibrationTime;

    private double startLat;
    private double startLong;
    LatLng startLatLong;
    int across;
    int width;


    private int facing ;

    public Riders(GoogleMap googleMap, JSONObject riderObject) throws JSONException{
        across = -1 ;
        width = -1 ;

        initialize(googleMap, riderObject);
        facing = 1;

    }



    public void initialize(GoogleMap googleMap,JSONObject riderObject) throws JSONException{
        this.calibrationTime = -1;
        this.lastViewedPointIndex = 0;
        this.name = riderObject.getString(KEY_NAME);
        this.id = riderObject.getInt(KEY_ID);
        this.ondeliver = riderObject.getBoolean(KEY_ONDELIVERY);
        this.points = new ArrayList<>();
        setPoints(riderObject.getJSONArray(KEY_POSITIONS));
        this.googleMap = googleMap;
        this.drawnLine = new ArrayList<>();
        this.polyline = initPolyline();
        this.marker = initMarker();
        startLat = -1.4521406601747012;
        startLong = 36.67018163949251;
        startLatLong = new LatLng(startLat,startLong);

    }

    public int getId(){
        return this.id;
    }

    public void setRiderObject(JSONObject riderObject){
        try{
            JSONArray arr = riderObject.getJSONArray(KEY_POSITIONS);
            JSONObject b = arr.getJSONObject(0);
            double newLat,newLong;
            newLat = b.getDouble("lat");
            newLong = b.getDouble("long");
            LatLng latLng = new LatLng(newLat,newLong);

            int a = thisLat(newLat);
            int c = thisLong(newLong);

            if(across == -1 || width == -1 ){
                MyConstants.ourArray[a][c]++;
                across = a;
                width = c;
            }
            else if(across !=a || width != c ){
                MyConstants.ourArray[across][width]--;
                MyConstants.ourArray[a][c]++;
                across = a;
                width = c;
            }


            if(latLng != marker.getPosition() ){
                moveMarker(latLng,1000);
            }
            removeMarker();
            initialize(googleMap, riderObject);
        }catch(Exception e){
            e.printStackTrace();
        }

    }
    public int thisLat(double c){
        LatLng b;
        for(int i = 0 ; i < 10 ; i++){
            b = getDestinationPoint(startLatLong,0,((i+1)*5));
            if(b != null){
                if(c > startLat && c < b.latitude){
                    return (10-(i+1));
                }
            }
        }
        return 0;
    }
    public int thisLong(double c){
        LatLng b;
        for(int i = 0 ; i < 10 ; i++){
            b = getDestinationPoint(startLatLong,90,((i+1)*5));
            if(b!=null){
                if(c > startLong && c < b.longitude){
                    return i;
                }
            }

        }
        return 0;
    }
    private LatLng getDestinationPoint(LatLng source, double brng, double dist) {
        dist = dist / 6371;
        brng = Math.toRadians(brng);

        double lat1 = Math.toRadians(source.latitude), lon1 = Math.toRadians(source.longitude);
        double lat2 = Math.asin(Math.sin(lat1) * Math.cos(dist) +
                Math.cos(lat1) * Math.sin(dist) * Math.cos(brng));
        double lon2 = lon1 + Math.atan2(Math.sin(brng) * Math.sin(dist) *
                        Math.cos(lat1),
                Math.cos(dist) - Math.sin(lat1) *
                        Math.sin(lat2));
        if (Double.isNaN(lat2) || Double.isNaN(lon2)) {
            return null;
        }
        return new LatLng(Math.toDegrees(lat2), Math.toDegrees(lon2));
    }
    public void removeMarker(){
        marker.remove();
    }
    /**
     * Initializes the marker on the map corresponding to the current rider
     *
     * @return The marker object
     * @see com.google.android.gms.maps.model.Marker
     */

    private Marker initMarker() {//place the markers at the initial positions
        if(this.points.size() > 0 && this.googleMap != null) {
            Collections.sort(this.points, new Point.PointComparator());//make sure you sort the points ordered based on time




            Marker marker = googleMap.addMarker(new MarkerOptions().position(this.points.get(lastViewedPointIndex).getLatLng())
                    .title(this.name)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.motorbike_east)).alpha(0.7f));
            if(facing == 0){
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.motorbike_west));
            }

            if(ondeliver){
                marker.setTitle(name + " is on delivery");
            }
            else {
                marker.setTitle(name + " is free");
            }
            //marker.setVisible(false);//only unhide the marker when the rider starts moving
            marker.setVisible(true);
            return marker;
        }
        return null;
    }

    /**
     * Returns the time for the first point corresponding to this rider
     *
     * @return The Date object
     * @see java.util.Date
     */
    public long getStartTime() {
        Collections.sort(this.points, new Point.PointComparator());//make sure you sort the points ordered based on time
        return this.points.get(0).getTime().getTime();
    }

    /**
     * Returns the time for the last point corresponding to this rider
     *
     * @return The Date object
     * @see java.util.Date
     */
    public long getFinalTime() {
        Collections.sort(this.points, new Point.PointComparator());//make sure you sort the points ordered based on time
        return this.points.get(this.points.size() - 1).getTime().getTime();
    }

    /**
     * Sets the time difference (in milliseconds) between now and the actual time in which the location
     * data was captured
     *
     * @param calibrationTime   The time difference in milliseconds
     */
    public void setCalibrationTime(long calibrationTime) {
        this.calibrationTime = calibrationTime;
    }

    /**
     * Returns the time difference between when the simulation was started and when the data was
     * actually collected
     *
     * @return The time difference in milliseconds
     */
    public long getCalibrationTime() {
        return this.calibrationTime;
    }

    /**
     * Initializes the polyline representing the riders movement on the map
     *
     * @return The Polyline object
     * @see com.google.android.gms.maps.model.Polyline
     */
    private Polyline initPolyline() {
        if(this.googleMap != null && this.points.size() > 0 && this.drawnLine != null) {
            this.drawnLine.add(this.points.get(lastViewedPointIndex).getLatLng());
            Polyline polyline = googleMap.addPolyline(new PolylineOptions().addAll(drawnLine).width(10).color(getRandomColor()));
            //polyline.setVisible(false);//only unhide when the rider starts moving
            polyline.setVisible(true);
            return polyline;
        }
        return null;
    }

    /**
     * Returns a random dark color
     *
     * @return A Color object
     * @see android.graphics.Color
     */
    private int getRandomColor() {
        int r = 100 + (int)(Math.random()*255);
        int g = 100 + (int)(Math.random()*255);
        int b = 100 + (int)(Math.random()*255);

        return  Color.rgb(r, g, b);

    }

    /**
     * Initializes the points corresponding to this rider from the provided JSONArray
     *
     * @param pointArray    The JSONArray carrying the rider's points
     * @see org.json.JSONArray
     */
    public void setPoints(JSONArray pointArray) {

        for(int index = 0; index < pointArray.length(); index++) {
            try {
                //if(index == 0 && marker.getPosition() != pointArray.getJSONObject(index).)
                this.points.add(new Point(pointArray.getJSONObject(index)));
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG, "A JSON error occurred while trying to initialize a point in the points");
            } catch (ParseException e) {
                e.printStackTrace();
                Log.e(TAG, "A parsing error occurred while trying to initialize a point in the points");
            }
        }
    }

    /**
     * Animates the rider's position on the map to the current simulated time
     */
    public void animateToTime() {
        Date time = new Date();
        Collections.sort(points, new Point.PointComparator());//make sure that the points are ordered based on time
        for(int index = lastViewedPointIndex + 1; index < points.size(); index++) {
            //move the marker
            if(index > 0 && (calibrationTime + points.get(index).getTime().getTime()) <= time.getTime()) {
                long timeTaken = points.get(index).getTime().getTime() - points.get(index-1).getTime().getTime();
                if(timeTaken == 0){
                    timeTaken = 1000;
                }
                moveMarker(points.get(index).getLatLng(), timeTaken);
                lastViewedPointIndex = index;
            }
        }
    }

    /**
     * Performs the actual animation of moving the marker and polyline on the map
     *
     * @param toLatLng  The position on the map to send the marker
     * @param duration  The duration to take for the movement
     * @see com.google.android.gms.maps.model.LatLng
     */
    private void moveMarker(final LatLng toLatLng, final long duration) {
       // show();
        Log.d(TAG, "Moving marker to "+toLatLng.toString()+" in "+duration+ " for "+this.name);
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final LatLng startLatLng = marker.getPosition();
        final LinearInterpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                if((toLatLng.longitude - startLatLng.longitude) < 0) {
                    marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.motorbike_west));
                    facing = 0;
                }
                else if((toLatLng.longitude - startLatLng.longitude) > 0) {
                    marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.motorbike_east));
                    facing = 1;
                }
                if(ondeliver){
                    marker.setTitle(name + " is on delivery");
                }
                else{
                    marker.setTitle(name + " is free");
                }
                long elapsed = SystemClock.uptimeMillis() - start;

                float t = interpolator.getInterpolation((float) elapsed / duration);
                double lng = t * toLatLng.longitude + (1 - t) * startLatLng.longitude;
                double lat = t * toLatLng.latitude + (1 - t) * startLatLng.latitude;
                LatLng newPoint = new LatLng(lat, lng);
                if(marker.getPosition() != newPoint){
                    marker.setPosition(newPoint);
                }

                drawnLine.add(newPoint);
                polyline.setPoints(drawnLine);
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }
}