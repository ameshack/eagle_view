package eagle.sendy.co.ke.Depleted;

import android.os.Handler;

import com.google.android.gms.maps.GoogleMap;

import java.util.Timer;
import java.util.TimerTask;

import eagle.sendy.co.ke.Depleted.ViewController;

/**
 * Created by Derrick Nyakiba on 10/15/2015
 * derricknyakiba@gmail.com
 * Last Modified:10/15/2015 by Derrick Nyakiba
 */
public class MoveRiders {
    Timer timer;
    TimerTask timerTask;
    int counter = 1;
    final Handler handler = new Handler();
    GoogleMap googleMap;
    ViewController control;

    public MoveRiders(){
        control = new ViewController();
    }

    public void startTimer(GoogleMap gMap) {
        googleMap = gMap;
        ///set a new Timer
        timer = new Timer();
        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, after the first 5000ms the TimerTask will run every 10000ms
        timer.schedule(timerTask, 2000, 3000); //

    }

    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    public void run() {
                        counter++;
                        control.getRiderData(googleMap);
                    }
                });
            }
        };
    }
}
