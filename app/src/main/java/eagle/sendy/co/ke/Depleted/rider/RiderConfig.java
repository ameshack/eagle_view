package eagle.sendy.co.ke.Depleted.rider;

import org.json.JSONException;
import org.json.JSONObject;
import java.text.ParseException;
import java.util.Date;

import eagle.sendy.co.ke.helpus.MyConstants;
import eagle.sendy.co.ke.Depleted.model.Person;
import eagle.sendy.co.ke.Depleted.model.RiderPositions;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Derrick Nyakiba on 10/15/2015
 * derricknyakiba@gmail.com
 * Last Modified:10/15/2015 by Derrick Nyakiba
 */
public class RiderConfig {


    public void SetRider(final JSONObject personOjObject) {
          //clear any data from before


        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
              //  realm.createOrUpdateAllFromJson(Person.class, personOjObject.toString());
                Person people = realm.createObject(Person.class);
                try{
                    people.setName(personOjObject.getString("name"));
                    people.setOndelivery(personOjObject.getBoolean("ondelivery"));
                    people.setPhone(personOjObject.getString("phone"));
                    people.setRider_id(personOjObject.getInt("rider_id"));

                }
                catch(JSONException e){
                    e.printStackTrace();
                }
            }
         });
        realm.close();
    }
    public void SetRiderPositions(final JSONObject positionObject) {


        //clear any data from before
        //ClearPerson();
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RiderPositions positions= realm.createObject(RiderPositions.class);
                try{
                    positions.setCourse(positionObject.getInt("course"));
                    positions.setId(positionObject.getInt("Id"));
                    positions.setrLat(positionObject.getDouble("rLat"));
                    positions.setRlong(positionObject.getDouble("rLong"));
                    String dateString = positionObject.getString("rTime");
                    Date convertedDate = null;

                    // Creating SimpleDateFormat with yyyyMMdd format e.g."20110914"
                    try{
                        convertedDate =  MyConstants.sdf2.parse(dateString);
                    }catch(ParseException e){e.printStackTrace();}
                    positions.setrTime(convertedDate);
                }
                catch(JSONException e){
                    e.printStackTrace();
                }
                //realm.createAllFromJson(RiderPositions.class, positionObject.toString());
            }
        });
        realm.close();
    }

    public void ClearPositions(){
        Realm realm = Realm.getDefaultInstance();
        final RealmResults<RiderPositions> result = realm.where(RiderPositions.class).findAll();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                result.clear();
            }
        });
        realm.close();
    }
    public void ClearPeople(){
        Realm realm = Realm.getDefaultInstance();
        final RealmResults<Person> result = realm.where(Person.class).findAll();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                result.clear();
            }
        });
        realm.close();
    }

}
