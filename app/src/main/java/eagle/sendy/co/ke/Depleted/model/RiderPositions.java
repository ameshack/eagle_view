package eagle.sendy.co.ke.Depleted.model;

import java.util.Date;

import io.realm.RealmObject;

/**
 * Created by Derrick Nyakiba on 10/15/2015
 * derricknyakiba@gmail.com
 * Last Modified:10/15/2015 by Derrick Nyakiba
 */
public class RiderPositions extends RealmObject{

    private int Id;
    private double rLat;
    private double rlong;
    private int course;
    private Date rTime;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public double getrLat() {
        return rLat;
    }

    public void setrLat(double rLat) {
        this.rLat = rLat;
    }

    public double getRlong() {
        return rlong;
    }

    public void setRlong(double rlong) {
        this.rlong = rlong;
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public Date getrTime() {
        return rTime;
    }

    public void setrTime(Date rTime) {
        this.rTime = rTime;
    }
}
