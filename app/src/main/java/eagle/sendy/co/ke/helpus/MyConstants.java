package eagle.sendy.co.ke.helpus;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Polygon;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import eagle.sendy.co.ke.maps.Orders;
import eagle.sendy.co.ke.maps.Riders;

/**
 * Created by Derrick Nyakiba on 10/15/2015
 * derricknyakiba@gmail.com
 * Last Modified:10/28/2015 by Derrick Nyakiba
 * Class to contain all the constant data used in the application
 */
public class MyConstants {


    public static String mapUrl = "http://api.sendy.co.ke/parcel/api/v6/rider_map";//where to get the data
    public static double mylong = 36.871679;//default longitude
    public static double mylat =-1.240196;//default latitude
    public static int zoomlevel = 10;//default zoom level
    public static SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//the date format to be used
    public static int width = 15;
    public static int height = 15;
    public static int[][] ourArray = new int[width][height];
    public static GoogleMap googleMap;
    public static ArrayList<Orders> ourOrders = new ArrayList<>();
    public static ArrayList<Riders>riders = new ArrayList<>();
    public static int riderPlace = 0;
    public static int place = 0;
    public static Polygon[][] polygon = new Polygon[height][width];
    public static ArrayList<String> rem = new ArrayList<>();
    public static double startLat = -1.4521406601747012;
    public static double startLong = 36.67018163949251;
    public static double finalLat = -1.0021416601747012;
    public static double finalLong = 37.08018163949251;
    public static double diffLat = finalLat - startLat;
    public static double diffLong = finalLong - startLong;
    public static double divLat = diffLat / height;
    public static double divLong = diffLong / width;



}
