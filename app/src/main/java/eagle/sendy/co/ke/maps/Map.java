package eagle.sendy.co.ke.maps;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolygonOptions;

import eagle.sendy.co.ke.helpus.MyConstants;
import sendy.eagle_view.R;

/**
 * Created by Derrick Nyakiba on 10/15/2015
 * derricknyakiba@gmail.com
 * Last Modified:10/28/2015 by Derrick Nyakiba
 * Class that sets up the map and the divisions on it
 */
public class Map extends Fragment{
    View rootView;
    Bundle savedInstanceStatethis;
    LinearLayout mScrollView;

    WorkaroundMapFragment supportMapFragment;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
        savedInstanceStatethis=savedInstanceState;
        rootView = inflater.inflate(R.layout.activity_main, container, false);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (MyConstants.googleMap == null) {
            mScrollView = (LinearLayout) rootView.findViewById(R.id.llMap);
            supportMapFragment.setListener(new WorkaroundMapFragment.OnTouchListener() {
                @Override
                public void onTouch() {
                    mScrollView.requestDisallowInterceptTouchEvent(true);
                }
            });
            MyConstants.googleMap = supportMapFragment.getMap();
            MyConstants.googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            MyConstants.googleMap.setMyLocationEnabled(true);
            MyConstants.googleMap.getUiSettings().setZoomControlsEnabled(false);
            MyConstants.googleMap.getUiSettings().setMyLocationButtonEnabled(false);
            CameraPosition cameraPosition = new CameraPosition.Builder()//where the camera is positioned and at what zoom  level
                    .target(new LatLng(MyConstants.mylat, MyConstants.mylong))
                    .zoom(MyConstants.zoomlevel)
                    .build();
            MyConstants.googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));//place the camera to right position

            startPainting();
        }
    }
    public void startPainting(){
        for( int  i = 0 ; i < MyConstants.height ; i++ ){
            for(int j = 0 ; j < MyConstants.width ; j++ ){
                paintFirst((MyConstants.startLat + (MyConstants.divLat*j)),(MyConstants.startLat + (MyConstants.divLat*(j+1))),MyConstants.startLong + (MyConstants.divLong*i),MyConstants.startLong + (MyConstants.divLong*(i+1)),i,j);
            }
        }
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FragmentManager fm = getChildFragmentManager();
        supportMapFragment = (WorkaroundMapFragment) fm.findFragmentById(R.id.map);
        if (supportMapFragment == null) {
            supportMapFragment = new WorkaroundMapFragment();
            fm.beginTransaction().replace(R.id.map, supportMapFragment).commit();
        }
    }

    public void paintFirst(double upLat, double downLat,double leftLong,double rightLong,int i, int j){
        PolygonOptions rectOptions = new PolygonOptions()
                .add(new LatLng(upLat, leftLong),
                        new LatLng(downLat, leftLong),
                        new LatLng(downLat, rightLong),
                        new LatLng(upLat, rightLong))
                .strokeColor(Color.parseColor("#1892BF"))
                .strokeWidth(5);
        System.out.println("creating polygon for " + i + " " + j);
        System.out.println(upLat + " " + leftLong + "  to " + upLat + " " + rightLong + "  to " + downLat + " " + leftLong + "  to " + downLat + " " + rightLong);

        MyConstants.polygon[MyConstants.height-(j+1)][i] = MyConstants.googleMap.addPolygon(rectOptions);
    }
}
