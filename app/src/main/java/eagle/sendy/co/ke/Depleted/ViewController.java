package eagle.sendy.co.ke.Depleted;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Handler;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

import eagle.sendy.co.ke.helpus.MyConstants;
import eagle.sendy.co.ke.Depleted.rider.RiderConfig;

/**
 * Created by Derrick Nyakiba on 10/15/2015
 * derricknyakiba@gmail.com
 * Last Modified:10/15/2015 by Derrick Nyakiba
 */
public class ViewController{

    private ArrayList<Riders> riders;//list of riders
    GoogleMap googleMap;

    int num = 0;
    Boolean firstTime = true;
    double startLat;
    double startLong;
    double finalLat;
    double finalLong;
    double diffLat;
    double diffLong;
    Polygon[][] polygon = new Polygon[10][10];

    public ViewController(){
        riders = new ArrayList<>();
        startLat = -1.4521406601747012;
        startLong = 36.67018163949251;
        finalLat = -1.0021416601747012;
        finalLong = 37.08018163949251;
        diffLat = finalLat - startLat;
        diffLong = finalLong - startLong;
    }

    public void getRiderData(GoogleMap gMap){
        JSONObject parentServerRequest = new JSONObject();
        JSONObject serverRequest = new JSONObject();
        JSONObject requestParams = new JSONObject();
        googleMap =gMap;

        try{//to set up the request to be made to the server
            serverRequest.put("seconds",30);
            parentServerRequest.put("values",serverRequest);
            requestParams.put("request", parentServerRequest);
            requestParams.put("url", MyConstants.mapUrl);
            new AsyncTaskRiderData().execute(requestParams.toString());//to do the collecting of data in the background
        }
        catch(JSONException e){
            e.printStackTrace();
        }
        googleMap =gMap;
    }

    private class AsyncTaskRiderData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return  new Connection().HandShake(urls[0]);//get the data
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String json) { //process result
            JSONArray riderObjects;
            try {
                storeToDB(json);//store the results in the db
                JSONObject payload = new JSONObject(json);
                riderObjects = payload.getJSONArray("values");
                if(riderObjects != null) {
                    try {
                        long startTime = -1;
                        long endTime = -1;
                        if(firstTime){
                            for(int index = 0; index < riderObjects.length(); index++) {
                                Riders currRider = new Riders(googleMap, riderObjects.getJSONObject(index));
                                if(startTime == -1 || currRider.getStartTime() < startTime) {
                                    startTime = currRider.getStartTime();
                                }
                                if(endTime == -1 || currRider.getFinalTime() > endTime) {
                                    endTime = currRider.getFinalTime();
                                }
                                riders.add(currRider);
                                //riders.get(firstRiderIndex).show();
                            }
                            firstTime = false;
                        }
                        else{
                            for( int index = 0 ; index < riderObjects.length(); index++ ){
                                for(int j = 0 ; j < riders.size() ; j++){
                                    if(riders.get(j).getId() == riderObjects.getJSONObject(index).getInt("rider_id")){
                                        riders.get(j).setRiderObject(riderObjects.getJSONObject(index));
                                        riders.get(j).animateToTime();
                                        if(startTime == -1 || riders.get(j).getStartTime() < startTime) {
                                            startTime = riders.get(j).getStartTime();
                                        }
                                        if(endTime == -1 || riders.get(j).getFinalTime() > endTime) {
                                            endTime = riders.get(j).getFinalTime();
                                        }
                                    }
                                }
                            }
                        }
                        System.out.println("DERRICK");
                        for(int j = 0 ; j < 10 ; j++ ){
                            for(int k = 0 ; k < 10 ; k++ ){
                                System.out.print(MyConstants.ourArray[j][k] + "  ");
                            }
                            System.out.println();
                        }

                        if(num == 0){
                            for( int  i = 0 ; i < 10 ; i++ ){
                                for(int j = 0 ; j < 10 ; j++ ){
                                    double divLat = diffLat/10;
                                    double divLong = diffLong/10;
                                    paintFirst((startLat + (divLat*j)),(startLat + (divLat*(j+1))),startLong + (divLong*i),startLong + (divLong*(i+1)),i,j);
                                 }
                            }
                            num = 1;
                        }
                        else{
                            for(int i = 0 ; i < 10 ; i++ ){
                                for( int j = 0 ; j < 10 ; j++ ){
                                    if(MyConstants.ourArray[i][j]==0){
                                        paintMe(i,j,1);
                                    }
                                    else{
                                        paintMe(i,j,0);
                                    }
                                }
                            }
                        }
                        animatingMoves( startTime, endTime);//animate the movement of the riders
                    } catch (JSONException e) { e.printStackTrace(); }
                }
            } catch (JSONException e) {  e.printStackTrace(); }
        }
        public void paintMe(int i,int j,int add){
            int a = 0x40ff0000;
            System.out.println("The color is " + polygon[i][j].getFillColor() + " while a is " + a);
            Boolean d = polygon[i][j].getFillColor() == a;
            if(add == 1 && !d){
                polygon[i][j].setFillColor(0x40ff0000);
            }
            else if(add == 0){
                polygon[i][j].setFillColor(Color.TRANSPARENT);
            }
        }
        public void paintFirst(double upLat, double downLat,double leftLong,double rightLong,int i, int j){
            PolygonOptions rectOptions = new PolygonOptions()
                    .add(new LatLng(upLat, leftLong),
                            new LatLng(downLat, leftLong),
                            new LatLng(downLat, rightLong),
                            new LatLng(upLat, rightLong))
                    .strokeColor(Color.GREEN)
                    .strokeWidth(5);
            System.out.println("creating polygon for " + i + " " + j);
            System.out.println(upLat + " " + leftLong + "  to " + upLat + " " + rightLong + "  to " + downLat + " " + leftLong + "  to " + downLat + " " + rightLong);

            polygon[10-(j+1)][i] = googleMap.addPolygon(rectOptions);

        }

        public void animatingMoves(long startTime,long endTime){
            final long calibrationTime = new Date().getTime() - startTime;
            final Handler handler = new Handler();
            final int every = 1000;//1 second


            // googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(riders.get(firstRiderIndex).getCurrentLocation(), 9));
            Runnable recurrsiveCode = new Runnable() {
                @Override
                public void run() {
                    //animate movement for each of the riders
                    for(int index = 0; index < riders.size(); index++) {
                        //make sure the calibrated time is set on each of the riders before you start animating
                        if(riders.get(index).getCalibrationTime() == -1) {
                            riders.get(index).setCalibrationTime(calibrationTime);
                        }
                        riders.get(index).animateToTime();
                    }
                    handler.postDelayed(this, every);
                }
            };

            if(new Date().getTime() <= (endTime + calibrationTime)) {//only rerun the code if we still have points to show
                handler.postDelayed(recurrsiveCode, every);
            }

        }
        public void storeToDB(String json){

            try {
                new RiderConfig().ClearPeople();
                new RiderConfig().ClearPositions();
                JSONObject jObject = new JSONObject(json);
                if(jObject.getBoolean("status")){
                    JSONArray ridersArray = jObject.getJSONArray("values");

                    for( int  i = 0 ; i < ridersArray.length() ; i++ ){
                        JSONObject riderObject = ridersArray.getJSONObject(i);
                        JSONArray positionArray = riderObject.getJSONArray("positions");
                        JSONObject riderPositions =new JSONObject();
                        for(int j = 0 ; j < positionArray.length() ; j++ ){
                            JSONObject riderObjectPositions =positionArray.getJSONObject(j);
                            riderPositions.put("rLat",riderObjectPositions.getDouble("lat"));
                            riderPositions.put("Id",riderObject.getInt("rider_id"));
                            riderPositions.put("rLong", riderObjectPositions.getDouble("long"));
                            riderPositions.put("course", riderObjectPositions.getInt("course"));
                            riderPositions.put("rTime", riderObjectPositions.getString("time"));

                            new RiderConfig().SetRiderPositions(riderPositions);
                        }
                        new RiderConfig().SetRider(riderObject);
                    }
                }
            } catch (JSONException e) {e.printStackTrace();}
        }
    }
}

